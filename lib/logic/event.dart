import 'package:angular/core.dart';

import 'button.dart';
import 'cards.dart';

// Class for MainSlide, <main-slide></main-slide> "Carousel Slide"
@Injectable()
class MainSlideData {
  int _id;
  String _title;
  String _subTitle;
  List<imageCarousel> _carousels;
  Button _callToAction;
  MainSlideData.data(this._title, this._subTitle, this._carousels, this._callToAction, [this._id]);
  MainSlideData();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //carousels
  List<imageCarousel> get carousels => this._carousels;
  set carousels(List<imageCarousel> carousels) {
    this._carousels = carousels;
  }

  //callToAction
  Button get callToAction => this._callToAction;
  set callToAction(Button callToAction) {
    this._callToAction = callToAction;
  }
}

class imageCarousel {
  int _id;
  String _photo;
  String _title;
  String _subTitle;
  imageCarousel.data(this._title, this._subTitle, this._photo, [this._id]);
  imageCarousel();
  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //photo
  String get photo => this._photo;
  set photo(String images) {
    this._photo = images;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitles(String subTitle) {
    this._subTitle = subTitle;
  }
}

// Class for CountdownTimer <countdown-timer></countdown-timer>
@Injectable()
class CountdownTimerData {
  String _title;
  DateTime _eventDate;
  Button _callToAction;
  CountdownTimerData.data(this._title, this._eventDate, this._callToAction);
  CountdownTimerData();
  //title Name of event to show above timer
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //eventDate The date of event used to calc the count down timer
  DateTime get eventDate => this._eventDate;
  set eventDate(DateTime eventDate) {
    this._eventDate = eventDate;
  }

  //callToAction call to action for the user
  Button get callToAction => this._callToAction;
  set callToAction(Button callToAction) {
    this._callToAction = callToAction;
  }
}

// Class for Services <services></services>
@Injectable()
class ServicesData {
  String _title;
  AboutCards _cards;
  int _id;
  ServicesData.data(this._title, this._id, this._cards);
  ServicesData();
  //title Name of Event
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //Cards Information cards for the event
  AboutCards get Cards => this._cards;
  set Cards(AboutCards Cards) {
    this._cards = Cards;
  }

  //id Event id from api
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }
}

class AboutCards {
  Card card1;
  Card card2;
  Card card3;
  AboutCards(this.card1, this.card2, this.card3);
}

// Class for About <about></about>
@Injectable()
class AboutData {
  String _title;
  List<Card> _cards;
  AboutData.data(this._cards, this._title);
  AboutData();
  //title Event Title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //cards Cards about event
  List<Card> get cards => this._cards;
  set cards(List<Card> cards) {
    this._cards = cards;
  }
}

// Class for CounterSection <counter-section></counter-section>
@Injectable()
class CounterSectionData {
  int _id;
  List<CounterItem> _items;
  CounterSectionData.data(this._items, [this._id]);
  CounterSectionData();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //items List of CounterSection Counter items
  List<CounterItem> get items => this._items;
  set items(List<CounterItem> items) {
    this._items = items;
  }
}

class CounterItem {
  int _id;
  String _icon;
  String _title;
  String _subTitle;
  CounterItem.data(this._icon, this._title, this._subTitle, [this._id]);
  CounterItem();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //icon Location to Icon Sprite w/ size
  String get icon => this._icon;
  set icon(String icon) {
    this._icon = icon;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }
}

// Class for Schedules <schedules></schedules>
@Injectable()
class Schedules {
  int _id;
  String _title;
  String _subTitle;
  List<ScheduleNavSection> _navItems;
  List<ScheduleSection> _scheduleData;
  Schedules.data(this._title, this._subTitle, this._navItems, this._scheduleData, [this._id]);
  Schedules();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //navItems Nav data
  List<ScheduleNavSection> get navItems => this._navItems;
  set navItems(List<ScheduleNavSection> navItems) {
    this._navItems = navItems;
  }

  //scheduleData Schedule Data
  List<ScheduleSection> get scheduleData => this._scheduleData;
  set scheduleData(List<ScheduleSection> scheduleData) {
    this._scheduleData = scheduleData;
  }
}

class _items {
  int _id;
  String _title;
  String _subTitle;
  _items.data(this._title, this._subTitle, [this._id]);
  _items();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }
}

class ScheduleNavSection extends _items {
  String _navID;
  String _href;
  String _itemClass;
  ScheduleNavSection.data(this._navID, this._href, this._itemClass);
  ScheduleNavSection();

  //navID Navigation menu
  String get navID => this._navID;
  set navID(String navID) {
    this._navID = navID;
  }

  //href
  String get href => this._href;
  set href(String href) {
    this._href = href;
  }

  //itemClass
  String get itemClass => this._itemClass;
  set itemClass(String itemClass) {
    this._itemClass = itemClass;
  }
}

class ScheduleSection extends _items {
  String _navID;
  List<Card> _scheduleCards;
  ScheduleSection.data(this._navID, this._scheduleCards);
  ScheduleSection();
  //navID Nav id for the list of cards should match NavID from NavSection
  String get navID => this._navID;
  set navID(String navID) {
    this._navID = navID;
  }

  //scheduleCards The schedule cards list
  List<Card> get scheduleCards => this._scheduleCards;
  set scheduleCards(List<Card> scheduleCards) {
    this._scheduleCards = scheduleCards;
  }
}

// Class for Team <team></team>
@Injectable()
class Team {
  int _id;
  String _title;
  String _subTitle;
  List<TeamCard> _tcards;
  Team.data(this._title, this._subTitle, this._tcards, [this._id]);
  Team();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //tcards Team Cards for Team section
  List<TeamCard> get tcards => this._tcards;
  set tcards(List<TeamCard> tcards) {
    this._tcards = tcards;
  }
}

class SocialIcons {
  int _id;
  String _name;
  String _imageLocation;
  SocialIcons.data(this._name, this._imageLocation, [this._id]);
  SocialIcons();

  //name Social Network Name
  String get name => this._name;
  set name(String name) {
    this._name = name;
  }

  //imageLocation Sprite Icon location for the Social Network
  String get imageLocation => this._imageLocation;
  set imageLocation(String imageLocation) {
    this._imageLocation = imageLocation;
  }
}

class TeamCard extends Card {
  List<SocialIcons> _icons;
  TeamCard.Data(this._icons);
  TeamCard();

  //icons Social Icons location list
  List<SocialIcons> get icons => this._icons;
  set icons(List<SocialIcons> icons) {
    this._icons = icons;
  }
}

// Class for Gallery <gallery></gallery>
@Injectable()
class Gallery {
  int _id;
  String _title;
  String _subTitle;
  List<GalleryPhoto> _galleryPhotoList;
  Button _galleryButton;
  Gallery.data(this._title, this._subTitle, this._galleryPhotoList, this._galleryButton, [this._id]);
  Gallery();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //galleryPhotoList Collection of photos and icons
  List<GalleryPhoto> get galleryPhotoList => this._galleryPhotoList;
  set galleryPhotoList(List<GalleryPhoto> galleryPhotoList) {
    this._galleryPhotoList = galleryPhotoList;
  }

  //galleryButton
  Button get galleryButton => this._galleryButton;
  set galleryButton(Button galleryButton) {
    this._galleryButton = galleryButton;
  }
}

class GalleryPhoto {
  int _id;
  String _photo;
  String _icon;
  GalleryPhoto.data(this._photo, this._icon, [this._id]);
  GalleryPhoto();
  //id Gallery Photo ID
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //photo Image location
  String get photo => this._photo;
  set photo(String photo) {
    this._photo = photo;
  }

  //icon Icon location from sprite
  String get icon => this._icon;
  set icon(String icon) {
    this._icon = icon;
  }
}

// Class for Faq <faq></faq>
@Injectable()
class Faq {
  int _id;
  String _title;
  String _subTitle;
  List<QuestionCard> _questions;
  Faq.data(this._title, this._subTitle, this._questions, [this._id]);
  Faq();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //questions Questions and answers list of cards
  List<QuestionCard> get questions => this._questions;
  set questions(List<QuestionCard> questions) {
    this._questions = questions;
  }
}

class QuestionCard {
  int _id;
  String _question;
  String _answer;
  String _icon;
  QuestionCard.data(this._question, this._answer, this._icon, [this._id]);
  QuestionCard();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //question
  String get question => this._question;
  set question(String title) {
    this._question = title;
  }

  //answer
  String get answer => this._answer;
  set answer(String subTitle) {
    this._answer = subTitle;
  }

  //icon Icon location in sprite
  String get icon => this._icon;
  set icon(String icon) {
    this._icon = icon;
  }
}

// Class for Sponsors <sponsors></sponsors>
@Injectable()
class Sponsors {
  int _id;
  String _title;
  String _subTitle;
  List<SponsorsCard> _sponsorList;
  Button _callToAction;
  Sponsors.data(this._title, this._subTitle, this._sponsorList, this._callToAction, [this._id]);
  Sponsors();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //callToAction Sponser call to action button
  Button get callToAction => this._callToAction;
  set callToAction(Button callToAction) {
    this._callToAction = callToAction;
  }

  //sponsorList List of sponsors Logos
  List<SponsorsCard> get sponsorList => this._sponsorList;
  set sponsorList(List<SponsorsCard> sponsorList) {
    this._sponsorList = sponsorList;
  }
}

class SponsorsCard {
  int _id;
  String _logoIcon;
  String _logo;
  SponsorsCard.data(this._logo, this._logoIcon, [this._id]);
  SponsorsCard();
  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //logoIcon Sprite Icon location
  String get logoIcon => this._logoIcon;
  set logoIcon(String logoIcon) {
    this._logoIcon = logoIcon;
  }

  //logo Logo file location ether png or svg
  String get logo => this._logo;
  set logo(String logo) {
    this._logo = logo;
  }
}

// Class for Pricing <pricing></pricing>
@Injectable()
class Pricing {
  int _id;
  String _title;
  String _subTitle;
  String _backgroundImage;
  List<PricingCard> _priceList;
  Pricing.data(this._title, this._subTitle, this._backgroundImage, this._priceList, [this._id]);
  Pricing();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //backgroundImage Parallax image for the pricing
  String get backgroundImage => this._backgroundImage;
  set backgroundImage(String backgroundImage) {
    this._backgroundImage = backgroundImage;
  }

  //priceList
  List<PricingCard> get priceList => this._priceList;
  set priceList(List<PricingCard> priceList) {
    this._priceList = priceList;
  }
}

class PricingCard extends Card {
  String _price;
  String _icon;
  Button _buyBtn;
  PricingCard.data(this._icon, this._price, this._buyBtn);

  //price Item price
  String get price => this._price;
  set price(String price) {
    this._price = price;
  }

  //icon Location to Icon Sprite w/ size
  String get icon => this._icon;
  set icon(String icon) {
    this._icon = icon;
  }

  //buyBtn Call to action buy button
  Button get buyBtn => this._buyBtn;
  set buyBtn(Button buyBtn) {
    this._buyBtn = buyBtn;
  }
}

// Class for EventSlides <event-slides></event-slides>
@Injectable()
class EventSlides {
  int _id;
  String _title;
  String _subTitle;
  String _slideImage;
  String _message;
  List<EventSlidesChecklist> _checkList;
  EventSlides.data(this._title, this._subTitle, this._slideImage, this._message, this._checkList, [this._id]);
  EventSlides();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //slideImage Main slide image
  String get slideImage => this._slideImage;
  set slideImage(String slideImage) {
    this._slideImage = slideImage;
  }

  //message Main messaged for slide
  String get message => this._message;
  set message(String message) {
    this._message = message;
  }

  //checkList
  List<EventSlidesChecklist> get checkList => this._checkList;
  set checkList(List<EventSlidesChecklist> checkList) {
    this._checkList = checkList;
  }
}

class EventSlidesChecklist {
  int _id;
  String _icon;
  String _message;
  EventSlidesChecklist.data(this._icon, this._message, [this._id]);
  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //message Text for to display
  String get message => this._message;
  set message(String message) {
    this._message = message;
  }
}

// Class for Blog <blog></blog>
@Injectable()
class Blog {
  int _id;
  String _title;
  String _subTitle;
  Button _btnAction;
  List<BlogCard> _blogPosts;
  Blog.data(this._title, this._subTitle, this._btnAction, this._blogPosts, [this._id]);
  Blog();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //btnAction Call to action button
  Button get btnAction => this._btnAction;
  set btnAction(Button btnAction) {
    this._btnAction = btnAction;
  }

  //blogPosts List of blog posts
  List<BlogCard> get blogPosts => this._blogPosts;
  set blogPosts(List<BlogCard> blogPosts) {
    this._blogPosts = blogPosts;
  }
}

class BlogCard extends Card {
  String _tagMsg;
  String _dateMsg;
  String _commentsMsg;
  BlogCard.data(this._tagMsg, this._dateMsg, this._commentsMsg);
  BlogCard();
  //tagMsg Card message for tag
  String get tagMsg => this._tagMsg;
  set tagMsg(String tagMsg) {
    this._tagMsg = tagMsg;
  }

  //dateMsg Full date message
  String get dateMsg => this._dateMsg;
  set dateMsg(String dateMsg) {
    this._dateMsg = dateMsg;
  }

  //commentsMsg Name of author
  String get commentsMsg => this._commentsMsg;
  set commentsMsg(String commentsMsg) {
    this._commentsMsg = commentsMsg;
  }
}

// Class for Subscribe <subscribe></subscribe>
@Injectable()
class Subscribe {
  int _id;
  String _title;
  String _inputFieldMsg;
  Button _btnSubmit;
  Subscribe.data(this._title, this._inputFieldMsg, this._btnSubmit, [this._id]);
  Subscribe();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //inputFieldMsg Mesage for input field
  String get inputFieldMsg => this._inputFieldMsg;
  set inputFieldMsg(String inputFieldMsg) {
    this._inputFieldMsg = inputFieldMsg;
  }

  //btnSubmit Submit call to action button
  Button get btnSubmit => this._btnSubmit;
  set btnSubmit(Button btnSubmit) {
    this._btnSubmit = btnSubmit;
  }
}
