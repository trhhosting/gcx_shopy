import 'package:angular/core.dart';

@Injectable()
class Card {
  int _id;
  String _photo;
  String _title;
  String _subTitle;
  Map<String, String> _content;
  Card.data(this._photo, this._title, this._subTitle, this._content, [this._id]);
  Card();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //photo Card Image
  String get photo => this._photo;
  set photo(String photo) {
    this._photo = photo;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //content Misc Content
  Map<String, String> get content => this._content;
  set content(Map<String, String> content) {
    this._content = content;
  }
}
